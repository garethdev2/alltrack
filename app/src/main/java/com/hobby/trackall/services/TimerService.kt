package com.hobby.trackall.services

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.Handler
import android.os.HandlerThread
import android.os.IBinder
import com.hobby.trackall.activities.searchgroupsactivity.GenericCallback
import com.hobby.trackall.activities.timeblock.Clock
import com.hobby.trackall.activities.timeblock.TimerFragment
import java.util.concurrent.atomic.AtomicBoolean

class TimerService : Service() {
    val updateFrequency: Long = 100
    val onStopListeners = ArrayList<GenericCallback<Long>>()
    val onTimeUpdatedListeners = ArrayList<GenericCallback<Long>>()
    var onTimeBlockComplete: GenericCallback<Long>? = null
    private val binder = TimerBinder()
    private val clock = Clock()
    val timerThread = HandlerThread("TimerHandler")

    var timerHandler: Handler? = null

    val isStarted = AtomicBoolean(false)
    public var timeRemaining: Long = 0
    public var initialTime: Long = 0
    private val timeElapsed: Long
        get() = initialTime - timeRemaining

    private var runnable: UpdateTimeRunnable? = null

    inner class TimerBinder : Binder() {
        fun getService(): TimerService = this@TimerService
    }

    override fun onBind(intent: Intent): IBinder {
        return binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return super.onStartCommand(intent, flags, startId)
    }

    public fun addTimeUpdateListener(timeUpdateListener: GenericCallback<Long>) {
        onTimeUpdatedListeners.add(timeUpdateListener)
    }

    public fun removeTimeUpdateListener(timeUpdateListener: GenericCallback<Long>) {
        onTimeUpdatedListeners.remove(timeUpdateListener)
    }

    public fun addOnStopListener(onStopListener: GenericCallback<Long>) {
        onStopListeners.add(onStopListener)
    }

    public fun removeOnStopListener(onStopListener: GenericCallback<Long>) {
        onStopListeners.remove(onStopListener)
    }

    public fun stopTimer(){
        synchronized(isStarted) {
            isStarted.set(false)
            if (runnable != null) {
                timerHandler?.removeCallbacks(runnable)
            }
            for (listener in onStopListeners) {
                listener.onCallback(timeElapsed)
            }
            onTimeBlockComplete?.onCallback(timeElapsed)
            timeRemaining = 0L
            stopSelf()
        }
    }

    public fun startTimer() {
        synchronized(isStarted) {
            isStarted.set(true)
            val startTime = clock.elapsedTime()
            if (runnable != null) {
                timerHandler?.removeCallbacks(runnable)
            }
            runnable = UpdateTimeRunnable(startTime + timeRemaining)
            updateTime(runnable!!)
        }
    }

    public fun pauseTimer() {
        synchronized(isStarted) {
            isStarted.set(false)
            if (runnable != null) {
                timerHandler?.removeCallbacks(runnable)
            }
        }
    }

    public fun initialize(time: Long) {
        synchronized(isStarted) {
            initialTime = time
            timeRemaining = time
            isStarted.set(false)
            if (runnable != null) {
                timerHandler?.removeCallbacks(runnable)
            }
            if (timerHandler == null) {
                timerThread.start()
                timerHandler = Handler(timerThread.looper)
            }
        }
    }

    private inner class UpdateTimeRunnable(val finishTime: Long) : Runnable {
        override fun run() {
            updateTime(this)
        }
    }

    private fun updateTime(runnable: UpdateTimeRunnable) {
        synchronized(isStarted) {
            if (!isStarted.get()) {
                return
            }
            timeRemaining = Math.max(0, runnable.finishTime - clock.elapsedTime())

            if (timeRemaining == 0L) {
                stopTimer()
                return
            }

            for (listener in onTimeUpdatedListeners) {
                listener.onCallback(timeRemaining)
            }

            timerHandler?.postDelayed(runnable, updateFrequency)
        }
    }
}