package com.hobby.trackall.database.dbwrapper

import android.database.Cursor
import com.hobby.trackall.database.COLUMN_QUERY

class SearchQueriesCursor(query: Cursor) : CursorWrapper(query) {

    val searchQuery: String
        get() = getString(getColumnIndex(COLUMN_QUERY))
}