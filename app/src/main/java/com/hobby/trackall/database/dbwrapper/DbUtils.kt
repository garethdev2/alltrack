package com.hobby.trackall.database.dbwrapper

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.hobby.trackall.database.COLUMN_STORY_REF

class DbUtils {

    fun loadAllOfSomeTableWithStoryReference(
        readableDatabase: SQLiteDatabase,
        storyReference: Long,
        tableName: String
    ): Cursor {
        val selection = COLUMN_STORY_REF + "= ?"
        val selectionArgs = arrayOf("" + storyReference)
        return readableDatabase.query(
            tableName, null, selection,
            selectionArgs, null, null, null
        )
    }
}
