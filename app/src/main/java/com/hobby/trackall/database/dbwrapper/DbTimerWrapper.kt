package com.hobby.trackall.database.dbwrapper

import android.content.ContentValues
import android.database.Cursor
import com.hobby.trackall.database.*

class DbTimerWrapper(private val dbHelper: DbHelper) {

    fun insertTimer(timerLength: Long, timerName: String): Long {
        val db = dbHelper.getWritableDatabase()
        val values = ContentValues()
        values.put(COLUMN_TIMER_LENGTH, timerLength)
        values.put(COLUMN_TIMER_NAME, timerName)
        return db.insert(TABLE_TIMER, null, values)
    }

    fun deleteTimer(timerId: Long) {
        val db = dbHelper.getWritableDatabase()
        db.delete(
            TABLE_TIMER,
            COLUMN_ID + "=?",
            arrayOf("" + timerId)
        )
    }

    fun loadAllTimers(): Cursor {
        return dbHelper.getReadableDatabase()
            .query(TABLE_TIMER, null, null, null, null, null, null)
    }
}