package com.hobby.trackall.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

const val DATABASE_NAME = "trackall.db"
const val DATABASE_VERSION = 1

//TIMER TABLE
const val TABLE_TIMER = "timer"
const val COLUMN_TIMER_LENGTH = "timerlength"
const val COLUMN_TIMER_NAME = "timername"

const val COLUMN_ID = "_id"

//STORIES TABLE
const val TABLE_STORY = "stories"
const val COLUMN_TITLE = "title"
const val COLUMN_DESCRIPTION = "description"
const val COLUMN_STATE = "state"
const val COLUMN_POINTS = "points"

//TAGS TABLE (specific to table)
const val TABLE_TAG = "tag"
const val COLUMN_NAME = "name"

//NOTE TABLE (specific to note)
const val TABLE_NOTE = "note"
const val COLUMN_NOTE_TEXT = "notetext"


//TIMEBLOCK TABLE
const val TABLE_TIME_BLOCK = "timeblock"
const val COLUMN_TIME_START = "timestart"
const val COLUMN_TIME_END = "timeend"

//QUERY TABLE
const val TABLE_SEARCH_QUERY = "searchquery"
const val COLUMN_QUERY = "query"

//SHARED BETWEEN TAG AND NOTE TABLES
val COLUMN_STORY_REF = "storyreference"
val FOREIGN_KEY_STORY_REF = "foreign key(" + COLUMN_STORY_REF + ") references story(" + COLUMN_ID + ")"

open class DbHelper(context: Context) : SQLiteOpenHelper(context,
    DATABASE_NAME, null,
    DATABASE_VERSION
) {

    val DATABASE_CREATE_STORY = ("create table "
            + TABLE_STORY + "( "
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_TITLE + " text not null, "
            + COLUMN_DESCRIPTION + " text not null, "
            + COLUMN_POINTS + " integer, "
            + COLUMN_STATE + " text not null"
            + ");")

    val DATABASE_CREATE_TAG = ("create table "
            + TABLE_TAG + "( "
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NAME + " text not null, "
            + COLUMN_STORY_REF + " integer, "
            + FOREIGN_KEY_STORY_REF
            + ");")

    val DATABASE_CREATE_NOTE = ("create table "
            + TABLE_NOTE + "( "
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NOTE_TEXT + " text not null, "
            + COLUMN_STORY_REF + " integer, "
            + FOREIGN_KEY_STORY_REF
            + ");")

    val DATABASE_CREATE_TIME_BLOCK = ("create table "
            + TABLE_TIME_BLOCK + "( "
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_TIME_START + " integer not null, "
            + COLUMN_TIME_END + " integer not null, "
            + COLUMN_STORY_REF + " integer, "
            + FOREIGN_KEY_STORY_REF
            + ");")

    val DATABASE_CREATE_TIMER = ("create table "
            + TABLE_TIMER + "( "
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_TIMER_LENGTH + " integer not null, "
            + COLUMN_TIMER_NAME + " text not null "
            + ");")

    val DATABASE_CREATE_SEARCH_QUERY = ("create table "
            + TABLE_SEARCH_QUERY + "( "
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_QUERY + " text not null "
            + ");")


    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(DATABASE_CREATE_STORY)
        db?.execSQL(DATABASE_CREATE_TAG)
        db?.execSQL(DATABASE_CREATE_NOTE)
        db?.execSQL(DATABASE_CREATE_TIME_BLOCK)
        db?.execSQL(DATABASE_CREATE_TIMER)
        db?.execSQL(DATABASE_CREATE_SEARCH_QUERY)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
    }

}