package com.hobby.trackall.database.dbwrapper

import android.content.Context
import com.hobby.trackall.database.DbHelper

class DbWrapperFactory {
    fun createDbWrapper(context: Context): DbWrapper {
        val dbHelper = DbHelper(context)
        return DbWrapper(
            DbTimerWrapper(
                dbHelper
            ), dbHelper, DbUtils()
        )
    }
}