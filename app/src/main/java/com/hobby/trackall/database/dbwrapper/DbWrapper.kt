package com.hobby.trackall.database.dbwrapper

import android.content.ContentValues
import android.database.Cursor
import com.hobby.trackall.database.*
import java.util.*

open class DbWrapper(val timerDbWrapper: DbTimerWrapper, val dbHelper: DbHelper, val dbUtils: DbUtils) {

    fun insertStory(title: String, description: String, points: Int, state: String): Long {
        val db = dbHelper.writableDatabase
        val values = makeStoryContentValues(title, description, points, state)
        return db.insert(TABLE_STORY, null, values)
    }

    fun updateStory(storyId: Long, title: String, description: String, points: Int, state: String) {
        val db = dbHelper.writableDatabase
        val values = makeStoryContentValues(title, description, points, state)
        val whereClause = COLUMN_ID + "=?"
        val whereArgs = arrayOf("" + storyId)
        db.update(TABLE_STORY, values, whereClause, whereArgs)
    }

    open fun insertTag(name: String, storyReference: Long): Long {
        val db = dbHelper.writableDatabase
        val values = ContentValues()
        values.put(COLUMN_NAME, name.toLowerCase(Locale.US))
        values.put(COLUMN_STORY_REF, storyReference)
        return db.insert(TABLE_TAG, null, values)
    }

    fun insertNote(note: String, storyReference: Long): Long {
        val db = dbHelper.writableDatabase
        val values = ContentValues()
        values.put(COLUMN_NOTE_TEXT, note)
        values.put(COLUMN_STORY_REF, storyReference)
        return db.insert(TABLE_NOTE, null, values)
    }

    fun insertTimer(timerLength: Long, timerName: String): Long {
        return timerDbWrapper.insertTimer(timerLength, timerName)
    }

    fun insertSearchQuery(query: String): Long {
        val db = dbHelper.writableDatabase
        val values = ContentValues()
        values.put(COLUMN_QUERY, query)
        return db.insert(TABLE_SEARCH_QUERY, null, values)
    }

    /*TODO: Return an extension of Cursor with utility methods
    such as cursor.getLong(COLUMN_TIME_START)
    or even: storiesCursor.getPoints(), timesCursor.getStartTime()*/
    fun loadAllStories(): Cursor {
        val db = dbHelper.readableDatabase
        return db.query(TABLE_STORY, null, null, null, null, null, null)
    }

    fun loadAllTagsForStory(storyReference: Long): Cursor {
        return loadAllOfSomeTableForStory(storyReference, TABLE_TAG)
    }

    fun loadAllSearchQueries(): SearchQueriesCursor {
        val db = dbHelper.readableDatabase
        return SearchQueriesCursor(
            db.query(
                TABLE_STORY,
                null,
                null,
                null,
                null,
                null,
                null
            )
        )
    }

    fun loadAllNotesForStory(storyReference: Long): Cursor {
        return loadAllOfSomeTableForStory(storyReference, TABLE_NOTE)
    }

    fun loadAllTimeBlocksForStory(storyReference: Long): Cursor {
        return loadAllOfSomeTableForStory(storyReference, TABLE_TIME_BLOCK)
    }

    fun deleteStory(storyId: Long) {
        val db = dbHelper.writableDatabase
        val whereClause = COLUMN_ID + "=?"
        val whereArgs = arrayOf("" + storyId)
        db.delete(TABLE_STORY, whereClause, whereArgs)
        deleteNotesForStory(storyId)
        deleteTagsForStory(storyId)
    }

    private fun makeStoryContentValues(title: String, description: String, points: Int, state: String): ContentValues {
        val values = ContentValues()
        values.put(COLUMN_TITLE, title)
        values.put(COLUMN_DESCRIPTION, description)
        values.put(COLUMN_POINTS, points)
        values.put(COLUMN_STATE, state)
        return values
    }

    fun deleteNotesForStory(storyReference: Long) {
        deleteAllOfSomeTableForStory(storyReference, TABLE_NOTE)
    }

    fun deleteTagsForStory(storyReference: Long) {
        deleteAllOfSomeTableForStory(storyReference, TABLE_TAG)
    }

   fun deleteAllOfSomeTableForStory(storyReference: Long, tableName: String) {
        val db = dbHelper.writableDatabase
        val whereClause = COLUMN_STORY_REF + "=?"
        val whereArgs = arrayOf("" + storyReference)
        db.delete(tableName, whereClause, whereArgs)
    }

    fun loadAllOfSomeTableForStory(
        storyReference: Long,
        tableName: String
    ): Cursor {
        return dbUtils.loadAllOfSomeTableWithStoryReference(
            dbHelper.readableDatabase,
            storyReference,
            tableName
        )
    }

    fun loadAllTimers(): Cursor {
        return timerDbWrapper.loadAllTimers()
    }

    fun deleteTimer(timerId: Long) {
        timerDbWrapper.deleteTimer(timerId)
    }

    fun loadStory(storyId: Long): Cursor {
        val db = dbHelper.readableDatabase
        val selection = "$COLUMN_ID=?"
        val selectionArgs = arrayOf("" + storyId)
        return db.query(TABLE_STORY, null, selection, selectionArgs, null, null, null)
    }

    open fun deleteTag(tagName: String, storyId: Long): Int {
        val db = dbHelper.writableDatabase
        val whereClause = "$COLUMN_STORY_REF=? AND $COLUMN_NAME=?"
        val whereArgs = arrayOf("" + storyId, tagName)
        return db.delete(TABLE_TAG, whereClause, whereArgs)
    }

    fun deleteSearchQuery(query: String): Int {
        val db = dbHelper.writableDatabase
        val whereClause = COLUMN_QUERY + "=?"
        val whereArgs = arrayOf(query)
        return db.delete(TABLE_SEARCH_QUERY, whereClause, whereArgs)
    }
}