package com.hobby.trackall.database.dbwrapper

import android.content.ContentValues
import android.database.Cursor
import com.hobby.trackall.database.*

class DbTimeBlockWrapper(private val dbHelper: DbHelper, private val dbUtils: DbUtils) {

    ///////////////////////////////////////////////////////////////////
    // PUBLIC METHODS
    ///////////////////////////////////////////////////////////////////
    fun insertTimeBlock(timeStart: Long, timeEnd: Long, storyReference: Long): Long {
        val db = dbHelper.getWritableDatabase()
        val values = makeTimeBlockContentValues(
            timeStart,
            timeEnd,
            storyReference
        )
        return db.insert(TABLE_TIME_BLOCK, null, values)
    }

    fun deleteTimeBlock(timeBlockId: Long) {
        val db = dbHelper.getWritableDatabase()
        val whereClause = COLUMN_ID + "=?"
        val whereArgs = arrayOf("" + timeBlockId)
        db.delete(TABLE_TIME_BLOCK, whereClause, whereArgs)
    }

    fun updateTimeBlock(timeBlockId: Long, startTime: Long, endTime: Long, storyReference: Long) {
        val db = dbHelper.getWritableDatabase()
        val values = makeTimeBlockContentValues(startTime, endTime, storyReference)
        val whereClause = COLUMN_ID + "=?"
        val whereArgs = arrayOf("" + timeBlockId)
        db.update(TABLE_TIME_BLOCK, values, whereClause, whereArgs)
    }

    fun loadAllTimeBlocksForStory(storyReference: Long): Cursor {
        return dbUtils.loadAllOfSomeTableWithStoryReference(
            dbHelper.getReadableDatabase(),
            storyReference,
            TABLE_TIME_BLOCK
        )
    }

    ///////////////////////////////////////////////////////////////////
    // PRIVATE METHODS
    ///////////////////////////////////////////////////////////////////
    private fun makeTimeBlockContentValues(timeStart: Long, timeEnd: Long, storyReference: Long): ContentValues {
        val values = ContentValues()
        values.put(COLUMN_TIME_START, timeStart)
        values.put(COLUMN_TIME_END, timeEnd)
        values.put(COLUMN_STORY_REF, storyReference)
        return values
    }
}
