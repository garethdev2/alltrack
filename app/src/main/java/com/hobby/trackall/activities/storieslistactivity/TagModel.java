package com.hobby.trackall.activities.storieslistactivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.hobby.trackall.R;
import com.hobby.trackall.activities.RecyclableModel;

/**
 * Created by g-dev on 2/27/17.
 */

public class TagModel implements RecyclableModel {
    public static final int TYPE = 2;
    private String tagLabel;
    private View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            return false;
        }
    };

    public TagModel(String tagLabel) {
        this.tagLabel = tagLabel;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new TagViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_tag_layout, null), onLongClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder) {
        TagViewHolder tagViewHolder = (TagViewHolder) viewHolder;
        tagViewHolder.tagLabelTextView.setText(tagLabel);
    }

    @Override
    public int getType() {
        return TYPE;
    }

    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.onLongClickListener = onLongClickListener;
    }

    public String getTagName() {
        return tagLabel;
    }

    public static class TagViewHolder extends RecyclerView.ViewHolder {
        TextView tagLabelTextView;
        public TagViewHolder(View itemView, View.OnLongClickListener onLongClickListener) {
            super(itemView);
            tagLabelTextView = (TextView) itemView.findViewById(R.id.tagTextView);
            itemView.setOnLongClickListener(onLongClickListener);
        }
    }
}
