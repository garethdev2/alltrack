package com.hobby.trackall.activities.timeblock

import android.os.SystemClock

class Clock {
    fun elapsedTime(): Long {
        return SystemClock.elapsedRealtime()
    }
}
