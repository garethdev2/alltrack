package com.hobby.trackall.activities.timeblock

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import butterknife.BindView
import butterknife.ButterKnife
import com.hobby.trackall.R
import com.hobby.trackall.activities.editstoryactivity.AddOrEditStoryActivity

class StoryTitleFragment : Fragment() {
    @BindView(R.id.textViewStoryTitle)
    internal var storyTitleTextView: TextView? = null

    private var args: Bundle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        args = getArguments()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.story_title_layout, container, false)
        storyTitleTextView = view.findViewById(R.id.textViewStoryTitle)
        ButterKnife.bind(this, view)

        val storyTitle = args!!.getString(STORY_TITLE_KEY)
        val storyId = args!!.getLong(STORY_ID_KEY)
        storyTitleTextView!!.text = storyTitle
        storyTitleTextView!!.setOnClickListener {
            val intent = Intent(getContext(), AddOrEditStoryActivity::class.java)
            intent.putExtra(AddOrEditStoryActivity.STORY_ID_KEY, storyId)
            getActivity()?.startActivityForResult(intent, EDIT_STORY_REQUEST)
        }
        return view
    }

    companion object {
        val STORY_TITLE_KEY = "STORY_TITLE_KEY"
        val STORY_ID_KEY = "STORY_ID_KEY"
        val EDIT_STORY_REQUEST = 79

        fun instantiate(storyTitle: String, storyId: Long): StoryTitleFragment {
            val args = Bundle()
            args.putString(STORY_TITLE_KEY, storyTitle)
            args.putLong(STORY_ID_KEY, storyId)
            val storyTitleFragment = StoryTitleFragment()
            storyTitleFragment.setArguments(args)
            return storyTitleFragment
        }
    }
}
