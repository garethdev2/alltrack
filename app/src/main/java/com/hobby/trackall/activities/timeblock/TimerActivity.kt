package com.hobby.trackall.activities.timeblock

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.hobby.trackall.R
import com.hobby.trackall.activities.storieslistactivity.StoriesListActivity
import com.hobby.trackall.database.DbHelper
import com.hobby.trackall.database.dbwrapper.DbTimeBlockWrapper
import com.hobby.trackall.database.dbwrapper.DbTimerWrapper
import com.hobby.trackall.database.dbwrapper.DbUtils
import com.hobby.trackall.database.dbwrapper.DbWrapper
import kotlinx.android.synthetic.main.activity_timer_layout.*
import java.util.*

class TimerActivity : AppCompatActivity() {

    lateinit var timerSidekick: TimerSidekick

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_timer_layout)

        val dbHelper = DbHelper(this)
        val dbUtils = DbUtils()
        val timerWrapper = DbTimerWrapper(dbHelper)
        val dbWrapper = DbWrapper(timerWrapper, dbHelper, dbUtils)
        val timeBlockWrapper = DbTimeBlockWrapper(dbHelper, dbUtils)

        val timerPagerAdapter = SimpleFragmentPagerAdapter(supportFragmentManager, ArrayList())
        val storyPagerAdapter = SimpleFragmentPagerAdapter(supportFragmentManager, ArrayList())

        timerSidekick = TimerSidekick(
            dbWrapper,
            timeBlockWrapper,
            selectedStoryViewPager,
            timerViewPager,
            timerPagerAdapter,
            storyPagerAdapter,
            AlertDialog.Builder(this),
            timerAddButton,
            timerDeleteButton,
            this
        )

        timerSidekick.onActivityCreate()
    }

    override fun onResume() {
        super.onResume()
        timerSidekick.onResume()
    }

    override fun onPause() {
        super.onPause()
        timerSidekick.onPause()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        timerSidekick.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.timer_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menuItem) {
            val intent = Intent(this, StoriesListActivity::class.java)
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item)
    }
}
