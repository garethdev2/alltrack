package com.hobby.trackall.activities.editstoryactivity;

import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.hobby.trackall.R;
import com.hobby.trackall.activities.RecyclableModel;
import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.FormatStyle;
import java.util.GregorianCalendar;

/**
 * Created by g-dev on 2/26/17.
 */

public class TimeBlockRecyclableModel implements RecyclableModel {
    public static final int TYPE_ID = 1;
    private long startTime;
    private long endTime;

    public TimeBlockRecyclableModel(long startTime, long endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent) {
        return new TimeBlockHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.time_block,
                parent,
                false));
    }

    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder viewHolder) {
        TimeBlockHolder timeBlockHolder = (TimeBlockHolder) viewHolder;
        Pair<String, String> startDateTime = formatTime(startTime);
        Resources resources = timeBlockHolder.textViewDate.getContext().getResources();
        String startTimeString = resources.getString(R.string.time_block_start_time, startDateTime.first);
        String endTimeString = resources.getString(R.string.time_block_end_time, formatTime(endTime).first);
        timeBlockHolder.textViewDate.setText(startDateTime.second);
        timeBlockHolder.textViewStartTime.setText(startTimeString);
        timeBlockHolder.textViewEndTime.setText(endTimeString);
    }

    private Pair<String, String> formatTime(long timeMillis) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(timeMillis);

        DateFormat fmt = SimpleDateFormat.getTimeInstance();
        fmt.setCalendar(calendar);
        String timeFormat = fmt.format(calendar.getTime());
        fmt = SimpleDateFormat.getDateInstance();
        fmt.setCalendar(calendar);
        String dateFormat = fmt.format(calendar.getTime());
        return new Pair<>(timeFormat, dateFormat);
    }

    @Override
    public int getType() {
        return TYPE_ID;
    }

    public static class TimeBlockHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewStartTime)
        TextView textViewStartTime;

        @BindView(R.id.textViewEndTime)
        TextView textViewEndTime;

        @BindView(R.id.textViewDate)
        TextView textViewDate;

        public TimeBlockHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
