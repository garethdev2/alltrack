package com.hobby.trackall.activities.storieslistactivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.hobby.trackall.R;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class StoryRecyclerAdapter extends RecyclerView.Adapter<StoryRecyclerAdapter.StoryViewHolder> {
    private List<StoryModel> storyModels;
    private DeleteButtonListener deleteClickListener;
    private ItemSelectedListener itemSelectedListener;

    public interface DeleteButtonListener {
        void onAdapterDeleteRequest(StoryModel storyModel);
    }

    public interface ItemSelectedListener {
        void onItemSelected(StoryModel storyModel);
    }

    public StoryRecyclerAdapter(List<StoryModel> storyModels, DeleteButtonListener deleteClickListener, ItemSelectedListener itemSelectedListener) {
        this.storyModels = storyModels;
        this.deleteClickListener = deleteClickListener;
        this.itemSelectedListener = itemSelectedListener;
    }

    @NotNull
    @Override
    public StoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cardView = LayoutInflater.from(parent.getContext()).inflate(R.layout.story_card, parent, false);
        return new StoryViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(final StoryViewHolder holder, int position) {
        final StoryModel storyModel = storyModels.get(position);
        holder.titleTextView.setText(storyModel.getTitle());
        holder.pointsTextView.setText(storyModel.getPoints() + " Points"); //TODO
        holder.stateTextView.setText(storyModel.getState());
        holder.deleteButton.setOnClickListener(v -> deleteClickListener.onAdapterDeleteRequest(storyModel));
        ViewGroup tagsLayout = holder.tagsLayout;
        tagsLayout.removeAllViewsInLayout();
        holder.itemView.setOnClickListener(v -> itemSelectedListener.onItemSelected(storyModel));
        for(String tag: storyModel.getTags()) {
            TextView tagTextView = (TextView) LayoutInflater.from(holder.itemView.getContext()).inflate(R.layout.single_tag_layout, tagsLayout, false);
            tagTextView.setText(tag);
            tagsLayout.addView(tagTextView);
        }
    }

    @Override
    public int getItemCount() {
        return storyModels.size();
    }

    public void setStoryModels(List<StoryModel> storyModels) {
        this.storyModels = storyModels;
    }

    static class StoryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.titleTextView)
        TextView titleTextView;
        @BindView(R.id.pointsTextView)
        TextView pointsTextView;
        @BindView(R.id.stateTextView)
        TextView stateTextView;
        @BindView(R.id.deleteButton)
        View deleteButton;
        @BindView(R.id.tagsLayout)
        ViewGroup tagsLayout;
        public StoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
