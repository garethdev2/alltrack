package com.hobby. trackall.activities.editstoryactivity

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.loader.app.LoaderManager
import androidx.loader.content.Loader
import androidx.recyclerview.widget.LinearLayoutManager
import com.hobby.trackall.R
import com.hobby.trackall.activities.RecyclableModel
import com.hobby.trackall.activities.RecyclableModelListAdapter
import com.hobby.trackall.database.DbHelper
import com.hobby.trackall.database.dbwrapper.DbTimerWrapper
import com.hobby.trackall.database.dbwrapper.DbUtils
import com.hobby.trackall.database.dbwrapper.DbWrapper
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import java.util.Arrays.asList
import kotlin.collections.ArrayList

class TimeBlocksListFragment : Fragment(), LoaderManager.LoaderCallbacks<List<RecyclableModel>>, LifecycleOwner {

    private var isFirstResumeAfterOnCreate = false
    var storyIds = ArrayList<Long>()
    val recyclableModelAdapter = RecyclableModelListAdapter(ArrayList())
    val loaderId = 3

    lateinit var dbWrapper: DbWrapper

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        storyCardsRecyclerView.layoutManager = LinearLayoutManager(activity)
        storyCardsRecyclerView?.adapter = recyclableModelAdapter
        if (arguments != null) {
            storyIds = ArrayList(asList(arguments!!.getLong(STORY_ID_KEY)))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val dbHelper = DbHelper(activity!!)
        val timerWrapper = DbTimerWrapper(dbHelper)
        dbWrapper = DbWrapper(timerWrapper, dbHelper, DbUtils())
        isFirstResumeAfterOnCreate = true
    }


    override fun onResume() {
        super.onResume()
        restartLoader()
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<List<RecyclableModel>> {
        val modelLoader = TimeBlocksModelLoader(activity!!, dbWrapper)
        modelLoader.storyIds = storyIds
        return modelLoader
    }

    override fun onLoadFinished(loader: Loader<List<RecyclableModel>>, data: List<RecyclableModel>) {
        recyclableModelAdapter.setRecyclableModels(data)
        recyclableModelAdapter.notifyDataSetChanged()
    }

    override fun onLoaderReset(loader: Loader<List<RecyclableModel>>) {
        Log.d("StoListActivity.reset", loader.toString())
        (loader as TimeBlocksModelLoader).storyIds = storyIds
    }

    private fun restartLoader() {
        val loader = LoaderManager.getInstance(this).getLoader<TimeBlocksModelLoader>(loaderId)
        if (loader?.isStarted != true) {
            LoaderManager.getInstance(this).initLoader(loaderId, null, this)
        }
        else if (loader?.isReset != true) {
            LoaderManager.getInstance(this).restartLoader(loaderId, null, this)
        }
    }

    companion object {

        val STORY_ID_KEY = "STORY_ID_KEY"

        fun instantiate(storyId: Long): TimeBlocksListFragment {
            val args = Bundle()
            args.putLong(STORY_ID_KEY, storyId)
            val timeBlocksListFragment = TimeBlocksListFragment()
            timeBlocksListFragment.setArguments(args)
            return timeBlocksListFragment
        }
    }
}