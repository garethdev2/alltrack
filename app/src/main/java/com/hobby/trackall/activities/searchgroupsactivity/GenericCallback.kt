package com.hobby.trackall.activities.searchgroupsactivity

interface GenericCallback<T> {
    fun onCallback(result: T)
}