package com.hobby.trackall.activities.timeblock

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import butterknife.BindView
import butterknife.BindViews
import butterknife.ButterKnife
import com.hobby.trackall.R
import kotlinx.android.synthetic.main.add_timer_activity.*
import java.util.*


class AddTimerActivity : AppCompatActivity() {
    private var digitButtons: List<TextView>? = null

    private var timeDigits: ArrayList<Int>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_timer_activity)
        digitButtons = ArrayList<TextView>(
            Arrays.asList(
                button0,
                button1,
                button2,
                button3,
                button4,
                button5,
                button6,
                button7,
                button8,
                button9
            )
        )

        initializeTimeQueue()
        initializeDigitButtons()
        initializeCancelButton()
        initializeAcceptButton()
        backspaceButton.setOnClickListener {
            timeDigits!!.removeAt(0)
            timeDigits!!.add(0)
            val newTime = convertTimeQueueToText(timeDigits!!)
            textViewTimeEntered!!.text = newTime
        }
    }

    private fun initializeAcceptButton() {
        buttonAcceptTimer.setOnClickListener {
            val data = Intent()
            val timeEntered = convertTimeQueueToMillis()
            data.putExtra(TIME_ENTERED_KEY, timeEntered)
            setResult(RESULT_OK, data)
            finish()
        }
    }

    private fun initializeCancelButton() {
        buttonCancelTimer.setOnClickListener {
            setResult(RESULT_CANCELED)
            finish()
        }
    }

    private fun convertTimeQueueToMillis(): Long {
        return timeDigits!![5].toLong() * 10 * HOURS +
                timeDigits!![4] * HOURS +
                timeDigits!![3].toLong() * 10 * MINUTES +
                timeDigits!![2] * MINUTES +
                timeDigits!![1].toLong() * 10 * SECONDS +
                timeDigits!![0] * SECONDS
    }

    private fun initializeTimeQueue() {
        timeDigits = ArrayList(6)
        for (i in 0..5) {
            timeDigits!!.add(0)
        }
    }

    private fun convertTimeQueueToText(timeQueue: ArrayList<Int>): String {
        return (timeQueue[5].toString() + "" + timeQueue[4] + ":"
                + timeQueue[3] + "" + timeQueue[2] + ":"
                + timeQueue[1] + "" + timeQueue[0])
    }

    private fun initializeDigitButtons() {
        val onDigitButtonListener = View.OnClickListener { view ->
            val clickedButton = view as TextView
            val buttonNumber = Integer.valueOf(clickedButton.text.toString())
            updateTimeEntered(buttonNumber)
        }

        for (digitButton in digitButtons!!) {
            digitButton.setOnClickListener(onDigitButtonListener)
        }
    }

    private fun updateTimeEntered(digitEntered: Int) {
        timeDigits!!.removeAt(5)
        timeDigits!!.add(0, digitEntered)
        val newText = convertTimeQueueToText(timeDigits!!)
        textViewTimeEntered!!.text = newText
    }

    companion object {
        val TIME_ENTERED_KEY = "TIME_ENTERED_KEY"
        private val SECONDS: Long = 1000
        private val MINUTES = 60 * SECONDS
        private val HOURS = 60 * MINUTES
    }
}
