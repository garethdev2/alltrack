package com.hobby.trackall.activities.searchgroupsactivity

import android.content.Intent
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.hobby.trackall.R
import com.hobby.trackall.activities.RecyclableModel
import com.hobby.trackall.activities.storieslistactivity.StoriesListActivity


class SearchGroupModel(private val label: String) : RecyclableModel {

    override val type: Int
        get() = "SearchGroupModel".hashCode()

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return SearchGroupViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.search_group_model_card_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder) {
        val searchGroupViewHolder = viewHolder as SearchGroupViewHolder
        searchGroupViewHolder.labelTextView!!.text = label
        searchGroupViewHolder.labelTextView!!.setOnClickListener { v ->
            val intent = Intent(v.context, StoriesListActivity::class.java)
            PreferenceManager.getDefaultSharedPreferences(v.context)
                .edit().putString("CURRENT_DEFAULT_SEARCH_QUERY", label).apply()
            v.context.startActivity(intent)
        }
    }

    internal class SearchGroupViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.groupNameTextView)
        var labelTextView: TextView? = null

        init {
            ButterKnife.bind(this, itemView)
        }
    }
}
