package com.hobby.trackall.activities.editstoryactivity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.SpinnerAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import butterknife.ButterKnife
import com.hobby.trackall.R
import com.hobby.trackall.activities.RecyclableModel
import com.hobby.trackall.activities.RecyclableModelListAdapter
import com.hobby.trackall.activities.storieslistactivity.TagModel
import com.hobby.trackall.activities.storieslistactivity.TagsController
import com.hobby.trackall.database.*
import com.hobby.trackall.database.dbwrapper.DbTimerWrapper
import com.hobby.trackall.database.dbwrapper.DbUtils
import com.hobby.trackall.database.dbwrapper.DbWrapper
import kotlinx.android.synthetic.main.edit_story_layout.*
import java.util.*
import java.util.Arrays.asList

class AddOrEditStoryActivity : AppCompatActivity() {

    private var storyId: Long = 0
    private var dbWrapper: DbWrapper? = null
    private var pointsAdapter: SpinnerAdapter? = null
    private var stateAdapter: SpinnerAdapter? = null
    private var tagsController: TagsController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_story_activity)

        Log.d(TAG, "AddOrEditStoryActivity started")
        pointsAdapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            ArrayList(asList("0", "1", "3", "5", "8"))
        )
        pointsSpinner.adapter = pointsAdapter

        stateAdapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            ArrayList(asList("Not Started", "Started", "Finished"))
        )
        stateSpinner.adapter = stateAdapter

        val dbHelper = DbHelper(this)
        val dbUtils = DbUtils()
        val timerWrapper = DbTimerWrapper(dbHelper)
        dbWrapper = DbWrapper(timerWrapper, dbHelper, dbUtils)

        matchViewToStoryId()
        saveButton.setOnClickListener {
            val data = saveStory()
            setResult(RESULT_OK, data)
            finish()
        }

        val timeBlocksFragment = TimeBlocksListFragment.instantiate(storyId)
        supportFragmentManager.beginTransaction()
            .add(R.id.timeBlocksFragment, timeBlocksFragment)
            .commit()
    }


    private fun matchViewToStoryId() {
        storyId = intent.getLongExtra(STORY_ID_KEY, -1)
        if (storyId != -1L) {
            val storyCursor = dbWrapper!!.loadStory(storyId)
            storyCursor.moveToFirst()
            if (storyCursor.isAfterLast) {
                return
            }
            titleEditText!!.setText(
                storyCursor.getString(storyCursor.getColumnIndex(COLUMN_TITLE))
            )
            descriptionEditText!!.setText(
                storyCursor.getString(storyCursor.getColumnIndex(COLUMN_DESCRIPTION))
            )
            setSpinnerState(
                pointsAdapter!!, pointsSpinner,
                "" + storyCursor.getInt(storyCursor.getColumnIndex(COLUMN_POINTS))
            )
            setSpinnerState(
                stateAdapter!!, stateSpinner,
                storyCursor.getString(storyCursor.getColumnIndex(COLUMN_STATE))
            )
            storyCursor.close()
        } else {
            saveStory()
        }

        val tagModels = ArrayList<TagModel>()
        val recyclableModels =
            ArrayList<RecyclableModel>() //(List<RecyclableModel>)((List) tagModels) TODO: Does this work?
        val tagsCursor = dbWrapper!!.loadAllTagsForStory(storyId)
        tagsCursor.moveToFirst()
        while (!tagsCursor.isAfterLast) {
            val tag = tagsCursor.getString(tagsCursor.getColumnIndex(COLUMN_NAME))
            val tagModel = TagModel(tag)
            tagModels.add(tagModel)
            recyclableModels.add(tagModel)
            tagsCursor.moveToNext()
        }
        tagsCursor.close()

        tagsController = TagsController(
            tagAddButton,
            tagModels,
            RecyclableModelListAdapter(recyclableModels),
            recyclerViewTags,
            dbWrapper,
            storyId
        )
        tagsController?.onCreate(this)
    }

    private fun saveStory(): Intent {
        val title = titleEditText!!.text.toString()
        val description = descriptionEditText!!.text.toString()
        val state = stateSpinner!!.selectedItem.toString()
        val points = Integer.parseInt(pointsSpinner!!.selectedItem.toString())
        Log.d(TAG, "title=$title, description=$description, state=$state, points=$points")
        if (storyId != -1L) {
            dbWrapper!!.updateStory(storyId, title, description, points, state)
        } else {
            storyId = dbWrapper!!.insertStory(title, description, points, state)
        }
        val data = Intent()
        data.putExtra(STORY_ID_KEY, storyId)
        return data
    }

    private fun setSpinnerState(spinnerAdapter: SpinnerAdapter, spinner: Spinner?, stateString: String) {
        for (i in 0 until spinnerAdapter.count) {
            val pointsAtPosition = spinnerAdapter.getItem(i).toString()
            if (pointsAtPosition == stateString) {
                spinner!!.setSelection(i)
            }
        }
    }

    companion object {
        val STORY_ID_KEY = "STORY_ID_KEY"
        private val TAG = AddOrEditStoryActivity::class.java.simpleName
    }
}
