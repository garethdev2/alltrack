package com.hobby.trackall.activities.storieslistactivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.hobby.trackall.R;
import com.hobby.trackall.activities.RecyclableModelListAdapter;
import com.hobby.trackall.database.dbwrapper.DbWrapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by g-dev on 2/27/17.
 */

public class TagsController {

    private Activity activity = null;
    private DbWrapper dbWrapper;
    private List<TagModel> tags;
    private RecyclableModelListAdapter adapter;
    private RecyclerView recyclerView;
    private View addTagButton;
    private long storyId;

    public TagsController(View addTagButton, List<TagModel> tags, RecyclableModelListAdapter adapter, RecyclerView recyclerView, DbWrapper dbWrapper, long storyId) {
        this.tags = tags;
        this.adapter = adapter;
        this.recyclerView = recyclerView;
        this.addTagButton = addTagButton;
        this.dbWrapper = dbWrapper;
        this.storyId = storyId;
    }

    public void onCreate(final Activity context) {
        //TODO: Move to factory.
        this.activity = context;
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);


        for (final TagModel tag : tags) {
            tag.setOnLongClickListener(getShowDeleteDialogClickListener(context));
        }

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        addTagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddTagDialog(v.getContext());
            }
        });
    }

    private View.OnLongClickListener getShowDeleteDialogClickListener(final Activity context) {
        return new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                TextView tagTextView = (TextView) v.findViewById(R.id.tagTextView);
                //Display dialog asking if the user would like to delete tag
                showDeleteTagDialog(context, tagTextView.getText().toString());
                return false;
            }
        };
    }

    private void showDeleteTagDialog(Context context, final String tagName) {
        new AlertDialog.Builder(context)
                .setCancelable(true)
                .setMessage("This action cannot be undone")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dbWrapper.deleteTag(tagName, storyId);
                        int preRemoveSize = tags.size();
                        int tagPosition = getTagStrings().indexOf(tagName);

                        adapter.removeRecyclableModel(tagPosition);
                        if(tags.size() == preRemoveSize) {
                            tags.remove(tagPosition);
                        }
                    }
                })
                .show();
    }

    private void showAddTagDialog(Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.add_tag_dialog_layout);
        final EditText tagNameEditText = (EditText) dialog.findViewById(R.id.tagNameEditText);
        View positiveButton = dialog.findViewById(R.id.add_tag_dialog_positive_button);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Add the tag to the database under the story id.
                String tagName = tagNameEditText.getText().toString().toLowerCase();
                List<String> tagStrings = getTagStrings();
                if(tagStrings.contains(tagName)) {
                    tagNameEditText.setError("Story has that tag already");
                    return;
                }
                dbWrapper.insertTag(tagNameEditText.getText().toString(), storyId);

                TagModel tagModel = new TagModel(tagName);
                tagModel.setOnLongClickListener(getShowDeleteDialogClickListener(activity));
                tags.add(tagModel);
                adapter.addRecyclableModel(tagModel, adapter.getItemCount());
                dialog.dismiss();
            }
        });
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    private List<String> getTagStrings() {
        List<String> tagStrings = new ArrayList<String>();
        for(TagModel tagModel: tags) {
            tagStrings.add(tagModel.getTagName());
        }
        return tagStrings;
    }
}
