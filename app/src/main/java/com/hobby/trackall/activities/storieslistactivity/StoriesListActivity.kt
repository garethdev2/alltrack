package com.hobby.trackall.activities.storieslistactivity

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import androidx.loader.app.LoaderManager
import androidx.loader.content.Loader
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.hobby.trackall.R
import com.hobby.trackall.activities.RecyclableModel
import com.hobby.trackall.activities.RecyclableModelListAdapter
import com.hobby.trackall.activities.editstoryactivity.AddOrEditStoryActivity
import com.hobby.trackall.activities.timeblock.TimerActivity
import com.hobby.trackall.database.DbHelper
import com.hobby.trackall.database.dbwrapper.DbTimerWrapper
import com.hobby.trackall.database.dbwrapper.DbUtils
import com.hobby.trackall.database.dbwrapper.DbWrapper
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_search_groups_layout.*
import java.util.*

class StoriesListActivity : AppCompatActivity(), LoaderManager.LoaderCallbacks<List<StoryModel>>, LifecycleOwner,
    StoryRecyclerAdapter.DeleteButtonListener, StoryRecyclerAdapter.ItemSelectedListener {

    private var dbWrapper: DbWrapper? = null
    private var storyRecyclerAdapter: StoryRecyclerAdapter? = null

    private var tagsController: TagsController? = null
    private var searchTags: HashSet<String>? = null

    internal var isFirstResumeAfterOnCreate = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)

        storyRecyclerAdapter = StoryRecyclerAdapter(ArrayList<StoryModel>(), this, this)
        storyCardsRecyclerView.layoutManager = LinearLayoutManager(this)
        storyCardsRecyclerView?.adapter = storyRecyclerAdapter

        storyAddButton?.setOnClickListener {
            val addStoryIntent = Intent(this@StoriesListActivity, AddOrEditStoryActivity::class.java)
            startActivity(addStoryIntent)
        }

        val dbHelper = DbHelper(this)
        val dbUtils = DbUtils()
        val timerWrapper = DbTimerWrapper(dbHelper)
        dbWrapper = DbWrapper(timerWrapper, DbHelper(this), DbUtils())

        val tags = ArrayList<TagModel>()
        //TODO: Consolidate with other tags
        searchTags = HashSet()
        val tagsAdapter = RecyclableModelListAdapter(ArrayList<RecyclableModel>())
        tagsController =
            TagsController(tagAddButton, tags, tagsAdapter, recyclerViewTags, object : DbWrapper(timerWrapper, dbHelper, dbUtils) {

                override fun deleteTag(tagName: String, storyId: Long): Int {
                    searchTags!!.remove(tagName)
                    runOnUiThread(Runnable { restartLoader() })
                    return 0
                }

                override fun insertTag(name: String, storyReference: Long): Long {
                    searchTags!!.add(name.toLowerCase(Locale.US))
                    runOnUiThread(Runnable { restartLoader() })
                    return 0

                }
            }, 0)
        tagsController!!.onCreate(this)
        LoaderManager.getInstance(this).initLoader(0, null, this)
        isFirstResumeAfterOnCreate = true
    }

    override fun onResume() {
        super.onResume()
        if (isFirstResumeAfterOnCreate) {
            isFirstResumeAfterOnCreate = false
            return
        }
        if (!getSupportLoaderManager().hasRunningLoaders()) {
            restartLoader()
        }
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<List<StoryModel>> {
        val modelLoader = StoryModelLoader(this, dbWrapper)
        modelLoader.setSearchTags(ArrayList(searchTags!!))
        return modelLoader
    }

    override fun onLoadFinished(loader: Loader<List<StoryModel>>, data: List<StoryModel>) {
        storyRecyclerAdapter!!.setStoryModels(data)
        storyRecyclerAdapter!!.notifyDataSetChanged()
    }

    override fun onLoaderReset(loader: Loader<List<StoryModel>>) {
        Log.d("StoListActivity.reset", loader.toString())
        (loader as StoryModelLoader).setSearchTags(ArrayList(searchTags!!))
    }

    override fun onAdapterDeleteRequest(storyModel: StoryModel) {
        AlertDialog.Builder(this)
            .setTitle("Delete this story?")
            .setMessage("This action cannot be undone.")
            .setPositiveButton("Delete", DialogInterface.OnClickListener { dialog, which ->
                dbWrapper!!.deleteStory(storyModel.getId())
                restartLoader()
                dialog.dismiss()
            })
            .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() }).create()
            .show()
    }

    override fun onItemSelected(storyModel: StoryModel) {
        val intent = Intent(this, AddOrEditStoryActivity::class.java)
        intent.putExtra(AddOrEditStoryActivity.STORY_ID_KEY, storyModel.getId())
        startActivity(intent) //TODO: Start activity for result so there's no need to restart the loader on every onResume??
    }

    private fun restartLoader() {
        getSupportLoaderManager().restartLoader(0, null, this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.timer_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menuItem) {
            val intent = Intent(this, TimerActivity::class.java)
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item)
    }
}