package com.hobby.trackall.activities.searchgroupsactivity

interface DataProvider<T> {

    fun provideData(callback: GenericCallback<T>)
}