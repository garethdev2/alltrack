package com.hobby.trackall.activities.editstoryactivity

import android.content.Context
import androidx.loader.content.AsyncTaskLoader
import com.hobby.trackall.activities.RecyclableModel
import com.hobby.trackall.database.COLUMN_ID
import com.hobby.trackall.database.COLUMN_TIME_END
import com.hobby.trackall.database.COLUMN_TIME_START
import com.hobby.trackall.database.dbwrapper.DbWrapper

class TimeBlocksModelLoader(
    context: Context,
    val dbWrapper: DbWrapper,
    var storyIds: ArrayList<Long> = ArrayList()
) : AsyncTaskLoader<List<RecyclableModel>>(context) {

    var mData: List<RecyclableModel>? = null

    override fun loadInBackground(): List<RecyclableModel>? {
        val data = java.util.ArrayList<TimeBlockRecyclableModel>()
        for (storyId in storyIds) {
            val timeBlockCursor = dbWrapper.loadAllTimeBlocksForStory(storyId)
            var hasNext = timeBlockCursor.moveToFirst()
            while (hasNext) {
                val id = timeBlockCursor.getLong(timeBlockCursor.getColumnIndex(COLUMN_ID))
                val startTime = timeBlockCursor.getLong(timeBlockCursor.getColumnIndex(COLUMN_TIME_START))
                val timeEnd = timeBlockCursor.getLong(timeBlockCursor.getColumnIndex(COLUMN_TIME_END))
                data.add(TimeBlockRecyclableModel(startTime, timeEnd))
                hasNext = timeBlockCursor.moveToNext()
            }
            timeBlockCursor.close()
        }
        return data
    }

    override fun deliverResult(data: List<RecyclableModel>?) {
        if (isReset) {
            return
        }

        mData = data

        if (isStarted) {
            super.deliverResult(data)
        }
    }

    override fun onStartLoading() {
        if (mData != null) {
            deliverResult(mData)
        }

        if (takeContentChanged() || mData == null) {
            forceLoad()
        }
    }

    override fun onStopLoading() {
        cancelLoad()
    }

    override fun onReset() {
        // Ensure the loader has been stopped.
        onStopLoading()

        if (mData != null) {
            mData = null
        }
    }

}