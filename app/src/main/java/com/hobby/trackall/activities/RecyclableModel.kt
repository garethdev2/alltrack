package com.hobby.trackall.activities

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

interface RecyclableModel {
    val type: Int

    fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder
    fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder)
}