package com.hobby.trackall.activities.timeblock

import android.app.Activity
import android.content.*
import android.database.Cursor
import android.os.IBinder
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.hobby.trackall.R
import com.hobby.trackall.activities.editstoryactivity.AddOrEditStoryActivity
import com.hobby.trackall.activities.searchgroupsactivity.GenericCallback
import com.hobby.trackall.database.COLUMN_ID
import com.hobby.trackall.database.COLUMN_TIMER_LENGTH
import com.hobby.trackall.database.COLUMN_TITLE
import com.hobby.trackall.database.dbwrapper.DbTimeBlockWrapper
import com.hobby.trackall.database.dbwrapper.DbWrapper
import com.hobby.trackall.services.TimerService
import java.util.ArrayList

class TimerSidekick(
    private val dbWrapper: DbWrapper,
    private val dbTimeBlockWrapper: DbTimeBlockWrapper,
    private val storyPager: ViewPager,
    private val timerPager: ViewPager,
    private val timerPagerAdapter: SimpleFragmentPagerAdapter,
    private val storyPagerAdapter: SimpleFragmentPagerAdapter,
    private val dialogBuilder: AlertDialog.Builder,
    private val addTimerButton: View,
    private val deleteTimerButton: View,
    private val activity: Activity
) : DialogInterface.OnClickListener, DialogInterface.OnCancelListener {

    override fun onClick(dialog: DialogInterface, which: Int) {
        if (which == DialogInterface.BUTTON_NEGATIVE) {
            discardTimeBlock()
//            onStopApproved!!.approveStop()
        } else if (which == DialogInterface.BUTTON_POSITIVE) {
            val timeElapsed = 0L//timerHelper!!.timeElapsed
            applyTimeToStory(timeElapsed)
//            onStopApproved!!.approveStop()
        }
    }

    fun onActivityCreate() {
        initializeTimerAdapter()
        initializeStoryAdapter()
        initializeTimerEditButtons()
    }

    private fun initializeTimerEditButtons() {
        addTimerButton.setOnClickListener {
            val addTimerActivityIntent = Intent(activity, AddTimerActivity::class.java)
            activity.startActivityForResult(addTimerActivityIntent, ADD_TIMER_REQUEST)
        }

        deleteTimerButton.setOnClickListener(View.OnClickListener {
            if (timerPagerAdapter.count <= 0) {
                return@OnClickListener
            }
            val currentTimerFragment = timerPagerAdapter.getItem(
                timerPager.getCurrentItem()
            ) as TimerFragment
            val timerId = currentTimerFragment.arguments
                ?.getLong(TimerFragment.TIMER_ID_KEY)
            if (timerId != null) dbWrapper.deleteTimer(timerId)
            timerPagerAdapter.removeFragment(timerPager.getCurrentItem())
        })
    }

    private fun initializeTimerAdapter() {
        val timerFragments = ArrayList<Fragment>()
        val timerCursor = dbWrapper.loadAllTimers()
        timerCursor.moveToFirst()
        while (!timerCursor.isAfterLast()) {
            val timerId = (extractFromCursor(timerCursor, COLUMN_ID) as Int).toLong()
            val timerLength = timerCursor.getLong(timerCursor.getColumnIndex(COLUMN_TIMER_LENGTH))
            timerFragments.add(TimerFragment.instantiate(timerLength, timerId))
            timerCursor.moveToNext()
        }
        timerCursor.close()
        timerPagerAdapter.setFragments(timerFragments)
        timerPager.setAdapter(timerPagerAdapter)
    }

    private fun initializeStoryAdapter() {
        val storyFragments = ArrayList<Fragment>()
        val storiesCursor = dbWrapper.loadAllStories()
        storiesCursor.moveToFirst()
        while (!storiesCursor.isAfterLast()) {
            val storyTitle = extractFromCursor(storiesCursor, COLUMN_TITLE) as String?
            val storyId = storiesCursor.getLong(storiesCursor.getColumnIndex(COLUMN_ID))
            storyFragments.add(StoryTitleFragment.instantiate(storyTitle!!, storyId))
            storiesCursor.moveToNext()
        }
        storiesCursor.close()
        if (storyFragments.size <= 0) {
            storyFragments.add(StoryTitleFragment.instantiate("No stories", -1))
        }
        storyPagerAdapter.setFragments(storyFragments)
        storyPager.setAdapter(storyPagerAdapter)
    }

    private fun extractFromCursor(cursor: Cursor, columnName: String): Any? {
        val columnIndex = cursor.getColumnIndex(columnName)
        val typeId = cursor.getType(columnIndex)
        if (typeId == Cursor.FIELD_TYPE_STRING) {
            return cursor.getString(columnIndex)
        } else if (typeId == Cursor.FIELD_TYPE_INTEGER) {
            return cursor.getInt(columnIndex)
        }
        return null
    }

    private fun discardTimeBlock() {

    }

    private fun applyTimeToStory(elapsedTime: Long) {
        val storyFragment = storyPagerAdapter.getItem(storyPager.getCurrentItem())
        val storyId = storyFragment.arguments?.getLong(StoryTitleFragment.STORY_ID_KEY)
        val currentTime = System.currentTimeMillis()
        dbTimeBlockWrapper.insertTimeBlock(currentTime - elapsedTime, currentTime, storyId!!)
    }

    override fun onCancel(dialog: DialogInterface) {
//        onStopApproved!!.cancelStop()
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == ADD_TIMER_REQUEST && resultCode == Activity.RESULT_OK) {
            val timerMillis = data!!.getLongExtra(AddTimerActivity.TIME_ENTERED_KEY, 0)
            if (timerMillis > 0) {
                val timerId = dbWrapper.insertTimer(timerMillis, "")
                val timerFragment = TimerFragment.instantiate(timerMillis, timerId)
                val location = Math.min(timerPager.getCurrentItem() + 1, timerPagerAdapter.count)
                timerPagerAdapter.addFragment(location, timerFragment)
                timerPager.setCurrentItem(location)
            }
        } else if (requestCode == StoryTitleFragment.EDIT_STORY_REQUEST && resultCode == Activity.RESULT_OK) {
            val storyId = data!!.getLongExtra(AddOrEditStoryActivity.STORY_ID_KEY, -1)
            val storyTitleFragment = createStoryFragment(storyId)
            if (storyTitleFragment != null) {
                if (!replaceNoStoryFragment(storyTitleFragment)) {
                    updateStoryFragment(storyTitleFragment)
                }
            }
        }
    }

    private fun updateStoryFragment(storyTitleFragment: StoryTitleFragment): Boolean {
        for (i in 0 until storyPagerAdapter.count) {
            val fragment = storyPagerAdapter.getItem(i)
            if (fragment.arguments?.getLong(StoryTitleFragment.STORY_ID_KEY) == storyTitleFragment.arguments?.getLong(
                    StoryTitleFragment.STORY_ID_KEY
                )
            ) {
                storyPagerAdapter.removeFragment(i)
                storyPagerAdapter.addFragment(i, storyTitleFragment)
                storyPager.setCurrentItem(i)
                return true
            }
        }
        return false
    }

    private fun replaceNoStoryFragment(storyTitleFragment: StoryTitleFragment): Boolean {
        if (storyPagerAdapter.count == 1 && storyPagerAdapter.getItem(0).arguments?.getLong(StoryTitleFragment.STORY_ID_KEY) == -1L) {
            storyPagerAdapter.removeFragment(0)
            storyPagerAdapter.addFragment(0, storyTitleFragment)
            return true
        }
        return false
    }

    private fun createStoryFragment(storyId: Long): StoryTitleFragment? {
        val storyCursor = dbWrapper.loadStory(storyId)
        storyCursor.moveToFirst()
        if (storyCursor.isAfterLast()) {
            storyCursor.close()
            return null
        }
        val title = storyCursor.getString(storyCursor.getColumnIndex(COLUMN_TITLE))
        storyCursor.close()
        return StoryTitleFragment.instantiate(title, storyId)

    }

    fun onResume() {
        val cursor = dbWrapper.loadAllStories()
        val storyCount = cursor.getCount()
        cursor.close()
        if (storyCount > storyPagerAdapter.count) {
            initializeStoryAdapter()
        }
    }

    fun onPause() {

    }

    companion object {
        val ADD_TIMER_REQUEST = 77
    }
}
