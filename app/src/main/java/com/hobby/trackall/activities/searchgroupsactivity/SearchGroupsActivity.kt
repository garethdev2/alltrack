package com.hobby.trackall.activities.searchgroupsactivity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import butterknife.ButterKnife
import com.hobby.trackall.R
import com.hobby.trackall.activities.RecyclableModel
import com.hobby.trackall.activities.RecyclableModelListAdapter
import com.hobby.trackall.database.dbwrapper.DbWrapperFactory
import java.util.*

class SearchGroupsActivity : AppCompatActivity() {
    private var recyclableModelListAdapter: RecyclableModelListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_groups_layout)
        recyclableModelListAdapter = RecyclableModelListAdapter(ArrayList<RecyclableModel>())
        ButterKnife.bind(this)
    }

    fun createDataProvider(): DataProvider<RecyclableModel> {
        val dbWrapper = DbWrapperFactory().createDbWrapper(this)
        return object : DataProvider<RecyclableModel> {
            override fun provideData(callback: GenericCallback<RecyclableModel>) {
                val cursor = dbWrapper.loadAllSearchQueries()
                var hasNext = cursor.moveToFirst()
                while (hasNext) {
                    recyclableModelListAdapter!!.addRecyclableModel(
                        SearchGroupModel(cursor.searchQuery),
                        recyclableModelListAdapter!!.getItemCount()
                    )
                    cursor.searchQuery
                    hasNext = cursor.moveToNext()
                }
            }
        }
    }
}