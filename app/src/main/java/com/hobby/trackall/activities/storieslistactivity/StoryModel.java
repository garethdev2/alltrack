package com.hobby.trackall.activities.storieslistactivity;

import java.io.Serializable;
import java.util.List;

public class StoryModel implements Serializable {
    private long id;
    private String title;
    private String description;
    private String state;
    private List<String> tags;
    private int points;

    public StoryModel(
            long id,
            String title,
            String description,
            String state,
            List<String> tags,
            int points
    ) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.tags = tags;
        this.points = points;
        this.state = state;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getTags() {
        return tags;
    }

    public int getPoints() {
        return points;
    }

    public long getId() {
        return id;
    }

    public String getState() {
        return state;
    }
}
