package com.hobby.trackall.activities.timeblock

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.hobby.trackall.R
import com.hobby.trackall.activities.searchgroupsactivity.GenericCallback
import com.hobby.trackall.database.DbHelper
import com.hobby.trackall.database.dbwrapper.DbTimeBlockWrapper
import com.hobby.trackall.database.dbwrapper.DbTimerWrapper
import com.hobby.trackall.database.dbwrapper.DbUtils
import com.hobby.trackall.database.dbwrapper.DbWrapper
import com.hobby.trackall.services.TimerService
import kotlinx.android.synthetic.main.timer_layout.*

class TimerFragment : Fragment(), View.OnClickListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.timer_card, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getLong(INITIAL_TIME_KEY)?.let { initialize(it) }
    }

    override fun onStart() {
        super.onStart()
        // Bind to LocalService
        Intent(activity, TimerService::class.java).also { intent ->
            activity?.bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onStop() {
        super.onStop()
        if (isBound) {
            activity?.unbindService(connection)
        }
        isBound = false
    }

    private var onStopGenericCallback = object : GenericCallback<Long> {
        override fun onCallback(result: Long) {
            if (timerService?.initialTime == arguments?.getLong(INITIAL_TIME_KEY)) {
                changeToStoppedState()
                val timeText = convertMillisecondsToChronoText(result)
                chronometer?.post { chronometer?.text = timeText }
            }
        }
    }

    private var onTimeBlockCompleted = object : GenericCallback<Long> {
        override fun onCallback(result: Long) {
            val storyId = arguments?.getLong(StoryTitleFragment.STORY_ID_KEY)
            val currentTime = System.currentTimeMillis()
            val dbHelper = activity?.let { DbHelper(it) }
            val dbUtils = DbUtils()
            val timeBlockWrapper = dbHelper?.let { DbTimeBlockWrapper(it, dbUtils) }
            timeBlockWrapper?.insertTimeBlock(currentTime - result, currentTime, storyId!!)
        }
    }

    private var onTimeUpdatedCallback = object : GenericCallback<Long> {
        override fun onCallback(result: Long) {
            if (timerService?.initialTime == arguments?.getLong(INITIAL_TIME_KEY)) {
                val timeText = convertMillisecondsToChronoText(result)
                chronometer?.post { chronometer?.text = timeText }
            } else {
                arguments?.getLong(INITIAL_TIME_KEY)?.let { initialize(it) }
            }
        }
    }

    fun initialize(time: Long) {
        chronometer?.post {
            chronometer?.text = convertMillisecondsToChronoText(time)
            startButton?.setOnClickListener(this)
            stopButton?.setOnClickListener(this)
            pauseResumeButton?.setOnClickListener(this)
            changeToStoppedState()
        }

    }

    override fun onClick(view: View) {
        if (view == startButton && isBound) {
            arguments?.getLong(INITIAL_TIME_KEY)?.let { initialize(it) }
            arguments?.getLong(INITIAL_TIME_KEY)?.let { timerService?.initialize(it) }
            chronometer.post { changeToStartedState() }
            timerService?.startTimer()
            timerService?.onTimeBlockComplete = onTimeBlockCompleted
        } else if (view == pauseResumeButton && timerService?.isStarted?.get() == false) {
            chronometer.post { changeToStartedState() }
            timerService?.startTimer()
        } else if (view == stopButton) {
            Log.d("TimerFragment onStop", "stopButton pressed")
            timerService?.stopTimer()
            arguments?.getLong(INITIAL_TIME_KEY)?.let { initialize(it) }
        } else if (view == pauseResumeButton && timerService?.isStarted?.get() == true) {
            timerService?.pauseTimer()
            chronometer.post { changeToPausedState() }
        } else {
            return
        }
    }

    private fun changeToPausedState() {
        pauseResumeButton?.post {
            pauseResumeButton?.setText(R.string.resume)
        }
    }

    private fun changeToStartedState() {
        startButton?.post {
            startButton?.visibility = View.GONE
            stopButton?.visibility = View.VISIBLE
            pauseResumeButton?.visibility = View.VISIBLE
            pauseResumeButton?.setText(R.string.pause)
        }
    }

    private fun changeToStoppedState() {
        startButton?.post {
            startButton?.visibility = View.VISIBLE
            stopButton?.visibility = View.GONE
            pauseResumeButton?.visibility = View.GONE
        }
    }

    private fun convertMillisecondsToChronoText(milliseconds: Long): String {
        val SECONDS = 1000
        val MINUTES = 60 * SECONDS
        val HOURS = 60 * MINUTES
        val hours = (milliseconds / HOURS).toInt()
        val timeRemainingMinusHours = (milliseconds % HOURS).toInt()
        val minutes = timeRemainingMinusHours / MINUTES
        val timeRemainingMinusHoursAndMinutes = timeRemainingMinusHours % MINUTES
        var seconds = timeRemainingMinusHoursAndMinutes / SECONDS
        if (milliseconds % 1000 > 0) {
            seconds++
        }
        return ((if (hours > 0) (hours / 10).toString() + "" + hours % 10 + ":" else "")
                + (if (minutes > 0 || hours > 0)
            (minutes / 10).toString() + "" + minutes % 10 + ":"
        else
            "")
                + seconds / 10 + "" + seconds % 10)
    }


    private var isBound = false
    var timerService: TimerService? = null
    private val connection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            Log.d("TimerFragment", "onServiceConnected")
            val binder = service as TimerService.TimerBinder
            timerService = binder.getService()
            isBound = true
            timerService?.addOnStopListener(onStopGenericCallback)
            timerService?.addTimeUpdateListener(onTimeUpdatedCallback)
            setViewAfterServiceConnect()
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            Log.d("TimerFragment", "onServiceDisconnected")
            isBound = false
            timerService?.removeOnStopListener(onStopGenericCallback)
            timerService?.removeTimeUpdateListener(onTimeUpdatedCallback)
        }
    }

    private fun setViewAfterServiceConnect() {
        if (timerService?.initialTime != arguments?.getLong(INITIAL_TIME_KEY)) {
            return
        }
        if (timerService?.isStarted?.get() == true) {
            chronometer.post { changeToStartedState() }
            val timeText = convertMillisecondsToChronoText(timerService!!.timeRemaining)
            chronometer.post { chronometer.text = timeText }
        }
    }

    companion object {

        val INITIAL_TIME_KEY = "INITIAL_TIME_KEY"
        val TIMER_ID_KEY = "TIMER_ID_KEY"

        fun instantiate(initialTime: Long, timerId: Long): TimerFragment {
            val args = Bundle()
            args.putLong(INITIAL_TIME_KEY, initialTime)
            args.putLong(TIMER_ID_KEY, timerId)
            val timerFragment = TimerFragment()
            timerFragment.setArguments(args)
            return timerFragment
        }
    }
}
