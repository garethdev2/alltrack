package com.hobby.trackall.activities.timeblock

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter


class SimpleFragmentPagerAdapter(
    private val fragmentManager: FragmentManager,
    private var fragments: MutableList<Fragment>?
) : FragmentStatePagerAdapter(fragmentManager) {


    override fun getCount(): Int {
        return fragments!!.size
    }

    override fun getItem(position: Int): Fragment {
        return fragments!![position]
    }

    override fun getItemPosition(`object`: Any): Int {
        // Causes adapter to reload all Fragments when
        // notifyDataSetChanged is called
        return POSITION_NONE
    }

    fun setFragments(fragments: MutableList<Fragment>) {
        this.fragments = fragments
        notifyDataSetChanged()
    }

    fun removeFragment(location: Int) {
        fragments!!.removeAt(location)
        notifyDataSetChanged()
    }

    fun addFragment(location: Int, fragment: Fragment) {
        fragments!!.add(location, fragment)
        notifyDataSetChanged()
    }
}
