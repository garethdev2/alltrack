package com.hobby.trackall.activities

import android.util.Log
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import java.util.HashMap


class RecyclableModelListAdapter(private val recyclableModels: MutableList<RecyclableModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun getItemCount(): Int {
        return recyclableModels.size
    }

    private val typeMap: MutableMap<Int, RecyclableModel>

    init {
        this.typeMap = HashMap()
        refreshTypeMap()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        Log.d("NullPointerException", "viewType=$viewType")
        val model = typeMap[viewType]
        return model!!.onCreateViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        recyclableModels[position].onBindViewHolder(holder)
    }

    override fun getItemViewType(position: Int): Int {
        return recyclableModels[position].type
    }

    fun addRecyclableModel(model: RecyclableModel, position: Int) {
        recyclableModels.add(position, model)
        refreshTypeMap()
        notifyItemInserted(position)
    }

    fun removeRecyclableModel(position: Int) {
        recyclableModels.removeAt(position)
        refreshTypeMap()
        notifyItemRemoved(position)
    }

    private fun refreshTypeMap() {
        typeMap.clear()
        for (recyclableModel in recyclableModels) {
            typeMap[recyclableModel.type] = recyclableModel
        }
    }

    fun setRecyclableModels(models: List<RecyclableModel>) {
        recyclableModels.clear()
        recyclableModels.addAll(models)
        refreshTypeMap()
        notifyDataSetChanged()
    }
}
