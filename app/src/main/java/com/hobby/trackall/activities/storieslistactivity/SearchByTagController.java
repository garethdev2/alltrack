package com.hobby.trackall.activities.storieslistactivity;

import com.hobby.trackall.database.dbwrapper.DbWrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by g-dev on 2/28/17.
 */

public class SearchByTagController {

    private Map<Character, List<String>> nextCharacterToTagName = new HashMap<>();
    private List<String> tagNames;
    private String charactersEntered;
    private List<String> tagsEntered;
    private DbWrapper dbWrapper;

    public SearchByTagController(List<String> tagNames, String charactersEntered) {
        this.tagNames = tagNames;
        this.charactersEntered = charactersEntered;
    }



    public void onTagEntered(String tagEntered) {
            
    }

    public void onCharacterEntered(Character characterEntered) {
        nextCharacterToTagName = getMapAfterEnteringCharacter(characterEntered, nextCharacterToTagName);
    }

    //TODO: Use this function!
    private Map<Character, List<String>> getMapAfterEnteringCharacter(Character characterEntered, Map<Character, List<String>> currentMap) {
        List<String> remainingTags = currentMap.get(characterEntered);
        Map<Character, List<String>> result = new HashMap<>();
        for(String tag: remainingTags) {
            updateResult(result, tag);
        }
        return result;
    }

    private void updateResult(Map<Character, List<String>> result, String tag) {
        if(tag == null) {
            return;
        }
        Character nextChar = tag.isEmpty() ? " ".charAt(0) : tag.charAt(0);
        String remainingUnmatchedTagChars = tag.isEmpty() ? "" : tag.substring(1);
        if(result.containsKey(nextChar)) {
            result.get(nextChar).add(remainingUnmatchedTagChars);
        } else {
            List<String> tagsWithNextChar = new ArrayList<>();
            tagsWithNextChar.add(remainingUnmatchedTagChars);
            result.put(nextChar, tagsWithNextChar);
        }
    }
}
