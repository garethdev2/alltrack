package com.hobby.trackall.activities.storieslistactivity;

import android.content.Context;
import android.database.Cursor;
import androidx.loader.content.AsyncTaskLoader;
import com.hobby.trackall.database.dbwrapper.DbWrapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.hobby.trackall.database.DbHelperKt.*;

public class StoryModelLoader extends AsyncTaskLoader<List<StoryModel>> {
    private List<String> searchTags = new ArrayList<>();
    private List<StoryModel> mData;
    private DbWrapper dbWrapper;

    public StoryModelLoader(Context ctx, DbWrapper dbWrapper) {
        super(ctx);
        this.dbWrapper = dbWrapper;
    }

    public void setSearchTags(List<String> searchTags) {
        this.searchTags = searchTags;
    }

    @Override
    public List<StoryModel> loadInBackground() {
        List<StoryModel> data = new ArrayList<>();
        Cursor storiesCursor = dbWrapper.loadAllStories();
        for (boolean hasNext = storiesCursor.moveToFirst(); hasNext; hasNext = storiesCursor.moveToNext()) {
            long id = storiesCursor.getLong(storiesCursor.getColumnIndex(COLUMN_ID));
            String title = storiesCursor.getString(storiesCursor.getColumnIndex(COLUMN_TITLE));
            String description = storiesCursor.getString(storiesCursor.getColumnIndex(COLUMN_DESCRIPTION));
            String state = storiesCursor.getString(storiesCursor.getColumnIndex(COLUMN_STATE));
            int points = storiesCursor.getInt(storiesCursor.getColumnIndex(COLUMN_POINTS));
            if(storyContainsAllSearchTags(id)) {
                data.add(new StoryModel(id, title, description, state, new ArrayList<String>(), points));
            }
        }
        storiesCursor.close();

        for (StoryModel model : data) {
            queryTagsAndAddToStoryModel(model);
        }
        return data;
    }

    private boolean storyContainsAllSearchTags(long id) {
        Cursor tagsCursor = dbWrapper.loadAllTagsForStory(id);
        Set<String> tagsLeftToMatch = new HashSet<>(searchTags);
        for (boolean hasNext = tagsCursor.moveToFirst(); hasNext; hasNext = tagsCursor.moveToNext()) {
            String tag = tagsCursor.getString(tagsCursor.getColumnIndex(COLUMN_NAME));
            tagsLeftToMatch.remove(tag);
        }
        tagsCursor.close();
        return tagsLeftToMatch.isEmpty();
    }

    private void queryTagsAndAddToStoryModel(StoryModel model) {
        //CAUSES MEMORY ERROR (make loading of tags implement cursor-recyclable
//        Cursor tagsCursor = dbWrapper.loadAllTagsForStory(model.getId());
//        List<String> modelTags = model.getTags();
//        for (boolean hasNext = tagsCursor.moveToFirst(); hasNext; hasNext = tagsCursor.moveToNext()) {
//            String tag = tagsCursor.getString(tagsCursor.getColumnIndex(DbHelper.COLUMN_NAME));
//            modelTags.add(tag);
//        }
//        tagsCursor.close();
    }

    @Override
    public void deliverResult(List<StoryModel> data) {
        if (isReset()) {
            return;
        }

        mData = data;

        if (isStarted()) {
            super.deliverResult(data);
        }
    }

    @Override
    protected void onStartLoading() {
        if (mData != null) {
            deliverResult(mData);
        }

        //TODO: Begin monitoring the underlying data source.
//    if (mObserver == null) {
//      mObserver = new SampleObserver();
//      // TODO: register the observer
//    }

        if (takeContentChanged() || mData == null) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    protected void onReset() {
        // Ensure the loader has been stopped.
        onStopLoading();

        if (mData != null) {
            mData = null;
        }

//    if (mObserver != null) {
//      // TODO: unregister the observer
//      mObserver = null;
//    }
    }

}

